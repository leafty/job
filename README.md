### To build image
```bash
make build
```

### Install dependencies in your machine:
```bash
make install
```

### Run tests in your machine:
```bash
make test
```

### Run tests in docker:
```bash
make test-in-docker
```

### To push the image
You must have `DOCKER_USERNAME` and `DOCKER_PASSWORD` available in the environment.
```bash
make push
```
